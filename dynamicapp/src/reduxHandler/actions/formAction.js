import { updateFieldDetails, updateFormDetails } from "./actionTypes";

export const addDetails = (val) => {
    return (dispatch, getState) => {
        let {formData}=getState();
        let index=formData.findIndex(sc=>sc.userName===val.userName);
        val.activeUser=true;
        if(index >-1){
          formData[index].activeUser=true;
        }
        let temp=index===-1?formData.push(val):formData;
        dispatch({type:updateFormDetails,payload:formData});
    };
  };
  export const update = (val) => {
    return (dispatch, getState) => { 
      dispatch({type:updateFormDetails,payload:val});
    };
  };
  export const updateData = (val) => {
    return (dispatch, getState) => { 
      dispatch({type:updateFieldDetails,payload:val});
    };
  };
  export const updateFormFieldData = () => {
    return (dispatch, getState) => { 
      let {formData,formFieldData}=getState();
      let index =formData.findIndex(sc=>sc.activeUser===true);
      formData[index].data=formFieldData;
      dispatch({type:updateFormDetails,payload:formData});
      dispatch({type:updateFieldDetails,payload:[]});
    };
  };
