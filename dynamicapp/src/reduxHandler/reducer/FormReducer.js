import { updateFieldDetails, updateFormDetails } from "../actions/actionTypes";

const initialState={
    formData:[],
    formFieldData:[]
}
const FormReducer = (state = initialState, action) => {
    switch (action.type) {
       case updateFormDetails:
      return {
        ...state,
        formData:[...action.payload]
      }; 
      
      case updateFieldDetails:
        return {
          ...state,
          formFieldData:[...action.payload]
        }; 
    default:
      return state;
  }
};
export default FormReducer;
