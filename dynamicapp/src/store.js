import { createStore,applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import FormReducer from "./reduxHandler/reducer/FormReducer";
const store = createStore(FormReducer,applyMiddleware(thunk));
store.subscribe(()=>{
   // console.log(store.getState());
});
export default store;
