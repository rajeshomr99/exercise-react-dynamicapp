import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux";
import { addDetails, update } from "../../reduxHandler/actions/formAction";
import { formPath } from "../../router/routerPath";
import { FileList } from "./fileList/FileList";
import { LoginModal } from "./formModal/LoginModal"
import { Header } from "./header/Header"

const FormLogin=(props)=>{
 const [modalCheck, setmodal] = useState(false);
 const[activeSave,setActive]=useState(true);
 const[formDetails,setForm]=useState({userName:"",desc:"",modal:false,activeUser:false,data:[],errorMsg:""});
 const formvalue = useSelector(state => state);
 const dispatch = useDispatch();
useEffect(() => {

    if(formvalue.formData.length > 0){
        formvalue.formData.map((item)=>
        {
            item.activeUser=false;
            item.modal=false;
        });
        dispatch(update(formvalue.formData));
    }
}, [])

    const handleModal=(value)=>{
        formvalue.formData.map((item)=>item.modal=false);
        dispatch(update(formvalue.formData));
        setmodal(value)
    }
    const handleChange=(e,name)=>{
        setForm({...formDetails,[name]:e.target.value,errorMsg:""})
        if(name==="userName"){
            setActive((e.target.value.match(/^[a-zA-Z]+$/)?false:true))
        }
        
    }
    const updateData=(e,name,option)=>{
        let index = formvalue.formData.findIndex(sc=>sc.userName===name);
        if(option==="delete"){
        formvalue.formData.splice(index,1);
        }
        else{
            formvalue.formData[index].activeUser=true;
            props.history.push(formPath);
        }
        dispatch(update(formvalue.formData))
    }
    const addFormDetails=(e)=>{
        e.preventDefault();
        let isvalid=formDetails.userName.length < 3? false:true;
        setForm({...formDetails,errorMsg:!isvalid?"Enter the value is atleast 3 character":""});
       
        if(isvalid){
            dispatch(addDetails(formDetails));
            props.history.push(formPath);
        }


    }
    const openSideMenu=(data,value)=>{
        formvalue.formData.map((item)=>item.modal=false);
        dispatch(update(formvalue.formData));
        let index = formvalue.formData.findIndex(sc=>sc.userName===data.userName); 
        console.log(index)
        formvalue.formData[index].modal=value?false:true;
        dispatch(update(formvalue.formData))
    }
    return(
        <div className="formLoginContainer">
            <Header Modal={handleModal}/>
            <LoginModal modalCheck={modalCheck} error={formDetails.errorMsg} addFormDetails={addFormDetails} activeSave={activeSave} changeValue={handleChange} Modal={handleModal} />
            <FileList formDetails={formvalue.formData} openSideMenu={openSideMenu} updateData={updateData} />
        </div>
    );
    

}
export default FormLogin;