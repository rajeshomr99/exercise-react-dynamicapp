import "./LoginModal.scss"
export const LoginModal=(props)=>{
    const {modalCheck,error, addFormDetails,changeValue,activeSave,Modal}=props;
 return(
     <>
     { modalCheck &&
     <div className="modal">
         <div className="modalContent">
         <div className="closeModal">
             <div className="close" onClick={()=>Modal(false)}> <i class="fas fa-times"></i></div>
             </div>
         <div className="formTitle"><b>Create Form</b> </div>
         <form onSubmit={(e)=>addFormDetails(e)}>
         <div className="formName">
             <div className="loginLabel">Form Name <span className="required">*</span></div>
             <input type="text" className="loginInput" maxLength="15" placeholder="Enter the Name" onChange={(e)=>changeValue(e,"userName")} /><br />
             <div className="errorMsg">{error}</div>
         </div>
         <div className="descrription">
         <div className="loginLabel">description</div>
             <textarea className="descrription-textarea" onChange={(e)=>changeValue(e,"desc")}></textarea>
         </div>
         <div className="submitButton">
             <input type="submit" className="save" disabled={activeSave} style={{backgroundColor:activeSave?"white":"blue"}} value="save" />
         </div>
         </form>
         </div>
     </div>
}
     </>
 );
}