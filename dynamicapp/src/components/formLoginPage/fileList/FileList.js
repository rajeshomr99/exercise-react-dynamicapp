import "./FileList.scss"
export const FileList=(props)=>{
    const {formDetails,openSideMenu,updateData}=props;
    return(
        <div className="">
            {formDetails.length===0 &&
            <div className="noFiles">
                <div className="filename">No Forms</div>
            </div>
        }
         {formDetails.length > 0 &&
            <div className="filelists">
                {formDetails.map((item)=>(
                <div className="filecontents">
                    <div className="formName">{item.userName}</div>
                    <div className="sidemenu" onClick={()=>openSideMenu(item,item.modal)}><i class="fas fa-ellipsis-v"></i></div>
                    {item.modal &&
                    <div className="deleteContent">
                        <div className="editTable" onClick={(e)=>updateData(e,item.userName,"edit")} >Edit</div>
                        <div className="editTable" onClick={(e)=>updateData(e,item.userName,"delete")}>Delete</div>
                    </div>
}
                </div>
                ))}
            </div>
        }

        </div>
    );
}