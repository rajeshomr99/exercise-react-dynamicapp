import "./Header.scss"
export const Header=(props)=>{
    const{Modal}=props;
    return(
        <div className="Header">
            <div className="title">Form</div>
            <div className="addFile" onClick={()=>Modal(true)}><i class="fas fa-plus"></i></div>
        </div>
    );
}