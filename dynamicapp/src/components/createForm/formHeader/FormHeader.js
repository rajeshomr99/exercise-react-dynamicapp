import "./FormHeader.scss"
export const  FormHeader=(props)=>{
    const{userName,saveData,goBack,formFieldData}=props;
    return(
        <div className="formHead">
            <div className='formName' ><b>{userName}</b></div>
            <div className="button">
                <input type="button" value="save" onClick={saveData} style={{backgroundColor:formFieldData.length > 0?"blue":"inherit"}}  className="saveForm"/>
                <input type='button' value="cancel" onClick={goBack} className="cancel   "/>
            </div>
        </div>
    );
}