import { advancedSideBar, sideBar } from "../../constants/constants";

export const SideBar=(props)=>{
    const{dropDown,currentField,formDetail,setRequiredSymbol,changeActive,onDragStart,activeField,changeDropDown}=props;
    const style={backgroundColor:"#e0e0e0",border: "2px dashed cornflowerblue",padding: "5px"};
    let checkedVale=currentField===null?false:formDetail[currentField].required;
   // console.log(checkedVale,currentField);
    return(
        <div className="sideBar">
                <div className="requiredField">
                    <div className="requiredFieldContents">
                        <div className="field" onClick={changeActive} style={!activeField?style:{}}>Field</div>
                        <div className='config' style={ activeField?style:{}}>configuration</div>
                    </div>
                    {activeField &&
                    <div className="requiredCheck">
                         <input type="checkbox" checked={checkedVale}  onChange={()=>setRequiredSymbol(formDetail[currentField].required)} /> 
                        <span>required field</span>
                    </div>
                    }
                </div>
                {!activeField &&
                <>
                {sideBar.map((item)=>(
                    <div className="sideContent" draggable  onDragStart={(e)=>onDragStart(e,item)}>
                        <div className ="logo"><span className="logoItems"><i className={item.logo}></i></span></div>
                        <div className="sideBar-Name">{item.label  }</div>
                      <div className="dragSymbol"><i class="fas fa-grip-vertical"></i></div>
                    </div>
                ))}
                <div className="advancesOption" onClick={changeDropDown}>
                    <div className="advance">Advanced Fieds</div>
                    <div className="dropSymbol" >{!dropDown ? <i class="fas fa-arrow-down"></i>:<i class="fas fa-arrow-up"></i>}</div>
                </div>
                {dropDown &&
                advancedSideBar.map((item)=>(
                    <div className="sideContent" draggable  onDragStart={(e)=>onDragStart(e,item)}>
                        <div className ="logo"><span className="logoItems"><i className={item.logo}></i></span></div>
                        <div className="sideBar-Name">{item.label}</div>
                        <div className="dragSymbol"><i class="fas fa-grip-vertical"></i></div>
                    </div>
                ))}
                
                </>
}
                </div>
    );
}