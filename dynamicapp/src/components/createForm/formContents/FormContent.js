
import { useEffect, useState } from "react";
import { DragContent } from "./dragContent";
import Image from "../../../asserts/image/nature.jpg"
import audio from "../../../asserts/audio/sweet.mp3"
import painovideo from "../../../asserts/video/pianovideo.mp4"
import "./FormContents.scss";
import { SideBar } from "./SideBar";
import { MobileContent } from "./MobileContent";
import { useDispatch, useSelector } from "react-redux";
import { updateData } from "../../../reduxHandler/actions/formAction";

export const FormContents=()=>{
    const[dropDowncheck,setDropDown]=useState(false);
    const [formDetail, setformDetail] = useState([]);
    const[activeField,setactive]=useState(false);
    const[currentField,setCurrentField]=useState(null);
    const dispatch = useDispatch();
     const details = useSelector(state => state);
     let formSet=details.formData; 
    useEffect(() => {
        dispatch(updateData(formDetail));   
        }, [formDetail]);
        useEffect(() => {
            let index =formSet.findIndex(sc=>sc.activeUser===true); 
            if(index > -1)
            {
             setformDetail([...formSet[index].data])
             }
            }, []);
    const changeDropDown=()=>{
        setDropDown(dropDowncheck?false:true);
      }
      const onDragStart = (ev, value) => {
        ev.dataTransfer.setData("dragContent",  JSON.stringify(value));
        ev.dataTransfer.setData("name", ev.target.className);
    }
    const onDragOver = (ev) => {
        ev.preventDefault();
    }
    const changeActive=()=>{
        setactive(false); 
    }

    const onDrop = (ev, item) => {
       ev.stopPropagation();
       let tagName=  ev.dataTransfer.getData("name");
       if(ev.target.className==="dragContainer" && tagName==="sideContent"){
        setactive(true);
        let id = JSON.parse(ev.dataTransfer.getData("dragContent"));
       let uniqueId=Math.random();
           formDetail.push({id:uniqueId,label:"",logo:id.logo,required:false,category:id.name,data:[],type:id.name==="number"?"number":id.name==="fileUpload"?"file":id.name==="date"?"date":id.name==="radio"?"radio":id.name==="dateTime"?"datetime-local":"text"})
          setformDetail([...formDetail])  ;
          let index=formDetail.findIndex(sc=>sc.id===uniqueId)
          setCurrentField(index);
       }
       else{
        setactive(false);
        let userData = JSON.parse(ev.dataTransfer.getData("dragContent"));  
        let deleteindex=formDetail.findIndex(sc=>sc.id===userData.id);
        let addindex =formDetail.findIndex(sc=>sc.id===item.id);
        if(addindex !==-1){
        formDetail.splice(deleteindex,1)
        formDetail.splice(addindex,0,userData)
       
        setformDetail([...formDetail])  ;
        }
        setCurrentField(addindex===-1?null:addindex)
       }

    }
    const handleChange=(e,id)=>{    
        let index =formDetail.findIndex(sc=>sc.id===id)
        formDetail[index].label=e.target.value;
        setformDetail([...formDetail])
    }
    const deleteElement=(e,id)=>{
        let index =formDetail.findIndex(sc=>sc.id===id)
        formDetail.splice(index,1);
        setformDetail([...formDetail])
        if(formDetail.length===0){
            setactive(false);
        }
        setCurrentField(null);
    }
    const addOption=(e,details)=>{
        const {id,category}=details;
        let index =formDetail.findIndex(sc=>sc.id===id)
        formDetail[index].data.push({id:Math.random(),name:"",label:"",category:category})
        setformDetail([...formDetail])  ;
    } 
    const addOptionvalue=(e,id,fieldId)=>{
        let index =formDetail.findIndex(sc=>sc.id===id);
        let fieldIndex=formDetail[index].data.findIndex(sc=>sc.id===fieldId);
        formDetail[index].data[fieldIndex].label=e.target.value;
        setformDetail([...formDetail])
    }
    const deleteOptionvalue=(e,id,fieldId)=>{
      console.log(id,fieldId);
      let index =formDetail.findIndex(sc=>sc.id===id);
      let fieldIndex=formDetail[index].data.findIndex(sc=>sc.id===fieldId);
       formDetail[index].data.splice(fieldIndex,1);
       setformDetail([...formDetail])
  }  
  const setRequiredSymbol=(value)=>{
    formDetail[currentField].required=value?false:true;
    setformDetail([...formDetail])
}   
const changeCurrent=(id)=>{
    setactive(true);
    let index=formDetail.findIndex(sc=>sc.id===id)
      setCurrentField(index);
}

    
    return(

        <div className="formContents" > 
            <SideBar onDragStart={onDragStart}  currentField={currentField} formDetail={formDetail} setRequiredSymbol={setRequiredSymbol} changeActive={changeActive} activeField={activeField}  dropDown={dropDowncheck} changeDropDown={changeDropDown}/>
            <DragContent onDragOver={onDragOver} onDragStart={onDragStart} changeCurrent={changeCurrent} deleteOptionvalue={deleteOptionvalue} addOptionvalue={addOptionvalue} addOption={addOption} deleteElement={deleteElement} formDetail={formDetail} editLabel={handleChange} onDrop={onDrop} />
            <MobileContent formDetail={formDetail}  Image={Image} video={painovideo} audio={audio} />
        </div>
    );
}