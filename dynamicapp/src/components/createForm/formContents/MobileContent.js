export const MobileContent=(props)=>{
    const {formDetail,video,audio,Image}=props;
    return(
    <div className=' mobileView'>
        <div className="mobilelayout">
          <div className="camera"></div>
          {formDetail.map((item)=>(
             <div className="mobileContent">
                <div className="label">{item.label} <span style={{color:"red    "}}>{item.required?"*":""}</span></div>
                 {(item.category ==="date"  ||item.category ==="dateTime"  ||  item.category==="textField" ||item.category ==="number" ||  item.category==="fileUpload") &&  
                <div className="fieldName">
                <input type={item.type} style={{width:"100%"}} className="mobileTextbox"/>
                </div>
                }
                {item.category==="image" &&  
                <div className="image">
                <img src={Image} alt="images" width="100%" height="100px" className="imageField" />
                </div>
                } 
                {
                    item.category==="multiLine" &&
                    <div>
                        <textarea className="textarea"></textarea>
                        </div>
                }
                {item.category==="audio" &&  
                <>
                <div className={item.category}>          
                      <audio src={audio} controls>
                </audio>
                </div>

                </>
                }
                {item.category==="video" &&  
                <div className={item.category}>
                <video src={video}  controls>
                    </video>
                </div>
                }
                {(item.data.length > 0 && item.category !=="dropDown") &&
                item.data.map((value)=>(
                    <div>
                        <input type={item.category==="multiselect"?"checkbox":"radio"} />
                        <span>{value.label}</span>
                        </div>
                ))}
                {item.category ==="dropDown" &&
                <div className={item.category}>
                <select className='selectField'>
                
                { item.data.map((value)=>(
                    <option>{value.label}</option>
                ))
                }
                </select>
                </div>
                }
             </div>
         ))}
            </div>
        </div>
);
};