import "./dragContents.scss";
export const DragContent=(props)=>{
    const{onDragOver, onDragStart,changeCurrent,addOption,deleteOptionvalue,addOptionvalue,editLabel,formDetail,deleteElement,onDrop}=props;
    console.log()
    return(
        <div className="dragContainer" onDragOver={(e)=>onDragOver(e)} onDrop={(e)=>onDrop(e,"c")} >
              {formDetail.map((item)=>
            <div className="dragcontent">
                <div className="fieldTag" draggable  onDragStart={(e)=>onDragStart(e,item)} onDragOver={(e)=>onDragOver(e)} onDrop={(e)=>onDrop(e,item)} >
                <div className="deleteItem" onClick={(e)=>deleteElement(e,item.id)}><i class="far fa-times-circle"></i></div>
                 <div className="logo_label" onClick={()=>changeCurrent(item.id)}>   
                <div className ="logo"><span className="logoItems"><i className={item.logo}></i></span></div>
                    <div className="editLabel">
                        <input type="text" className="changeLabel" value={item.label} onChange={(e)=>editLabel(e,item.id)} placeholder="enter the field"/>
                    </div>
                    <div className="dragSymbols"><i class="fas fa-grip-vertical"></i></div>
                    </div>
                    {(item.category==="multiselect"||item.category==="radio" ||item.category==="dropDown"  ) &&
                    <div className="addvalue">
                        <div className="add" onClick={(e)=>addOption(e,item)}><span className="addButton">+</span>Add Option
                        </div></div> 
                        }
                    {item.data.map((data)=>(
                        <div className="deletedata">
                            <input type="text" className="setLabel" value={data.label} onChange={(e)=>addOptionvalue(e,item.id,data.id)}/>
                            <div className="deletessubData" onClick={(e)=>deleteOptionvalue(e,item.id,data.id)}><i class="fas fa-trash"></i></div>
                        </div>
                    ))}
                </  div>
                    
                
            </div>
            )}
        </div>
    );

}