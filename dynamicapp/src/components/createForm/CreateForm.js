import { useState } from "react";
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux";
import { update, updateFormFieldData } from "../../reduxHandler/actions/formAction";
import { loginpath } from "../../router/routerPath";
import { FormContents } from "./formContents/FormContent";
import { FormHeader } from "./formHeader/FormHeader"

export const CreateForm=(props)=>{
    const [name, setName] = useState();
    const formDetails = useSelector(state => state);
    const{formData,formFieldData}=formDetails
    const dispatch = useDispatch();
    useEffect(() => {
        let check=formData.some((item)=>item.activeUser===true);
        if(formData.length ===0 ||!check){
            props.history.push(loginpath);
          }  
        setName(formData.find((item)=>item.activeUser===true)?.userName)
    }, []); 
    const saveData=()=>{
        if(formFieldData.length >0){
            dispatch(updateFormFieldData())
            props.history.push(loginpath);
        }
    }
    const deleteData=()=>{
        let index=formData.findIndex((item)=>item.activeUser===true);
        formData.splice(index,1);
        dispatch(update(formData))
        props.history.push(loginpath);
    }
return(
    
    <div>
        <FormHeader userName={name} goBack={deleteData} formFieldData={formFieldData} saveData={saveData}/>
        <FormContents />

    </div>
);
}