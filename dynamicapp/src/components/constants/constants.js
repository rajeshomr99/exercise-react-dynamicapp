export const sideBar=[{name:"textField",label:"Text Field",logo:`fas fa-tenge`},
{name:"number",label:"Number",logo:"fas fa-file-excel"},
{name:"multiselect",label:"Multi Select",logo:"fas fa-check-square"},
{name:"radio",label:"Radio",logo:"fab fa-algolia"},
{name:"dropDown",label:"DropDown",logo:"fas fa-chevron-circle-down"},
{name:"date",label:"Date",logo:"far fa-calendar-alt"}];

export const advancedSideBar=[{name:"image",label:"Image",logo:`far fa-image`},
{name:"video",label:"Video",logo:`fas fa-video`},
{name:"audio",label:"Audio",logo:`fas fa-file-audio`},
{name:"fileUpload",label:"File Upload",logo:`fas fa-file-upload`},
{name:"dateTime",label:"Date Time",logo:`fas fa-calendar-check`},
{name:"multiLine",label:"Multi Line",logo:`fas fa-sliders-h `},
]