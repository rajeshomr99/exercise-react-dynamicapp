import { CreateForm } from "../components/createForm/CreateForm";
import FormLogin from "../components/formLoginPage/FormLogin";
import { formPath, loginpath } from "./routerPath";

export const formRouter = [
  {
    path: loginpath,
    exact: true,
    component: FormLogin,
  },
  {
    path: formPath,
    exact: false ,
    component: CreateForm,
  },
];
